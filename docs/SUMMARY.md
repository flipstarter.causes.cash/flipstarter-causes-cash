# Summary

* [Welcome]()

    * [0.1. Readme](README.md)
    * [0.2. Foreword](FOREWORD.md)
    * [0.3. Changelog](CHANGELOG.md)
    * [0.4. Roadmap](ROADMAP.md)
    * [0.5. Contributing](CONTRIBUTING.md)

* [1. Campaigns]()

    * [1.1. Introduction](campaigns/intro.md)
    * [1.2. Direct Cash](campaigns/direct.md)
    * [1.3. Community Pledge](campaigns/community.md)
    * [1.4. Daily Payouts](campaigns/payouts.md)

* [2. Sponshorships]()

    * [2.1. What happens to the $$$?](sponsors/process.md)
    * [2.2. Petty (Bitcoin) Cash](sponsors/petty-cash.md)
    * [2.3. FREE as in beer](sponsors/free.md)
    * [2.4. Levels](sponsors/levels.md)

* [3. Community Funding]()

    * [3.1. Introduction](community/intro.md)
    * [3.2. Pay It Forward](community/pay-it-forward.md)
    * [3.3. PIF Token](community/PIF.md)

* [4. Teams]()

    * [4.1. Introduction](teams/intro.md)

* [5. Legal]()

    * [5.1. Code of Conduct](legal/coc.md)
    * [5.2. Terms of Use]()
    * [5.3. Privacy Notice]()
    * [5.4. Cookie Notice]()

* [6. Appendices]()

    * [6.1. Liberal Radicalism](appendices/liberal-radicalism.md)
    * [6.2. Mecenas (Oracle)](appendices/mecenas.md)
    * [6.3. Signed Seed Private Key](appendices/sspk.md)
